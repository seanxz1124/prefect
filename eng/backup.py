import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def daily_backup():
    if os.system('py F:/code/infra/daily_backup.py') != 0:
        raise RuntimeError


with Flow("backup") as flow:
    flow.add_task(daily_backup)

    # set up dependencies
    # schedule the flow
    schedule = Schedule(clocks=[CronClock("30 23 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
