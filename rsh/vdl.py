import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def insider_trading():
    if os.system('py F:/code/vdl/webscraper/insider_trading.py') != 0:
        raise RuntimeError


@task
def csi():
    if os.system('py F:/code/vdl/webscraper/csi.py') != 0:
        raise RuntimeError


@task
def executive_change():
    if os.system('py F:/code/vdl/jqdata/executive_change.py') != 0:
        raise RuntimeError


@task
def jq_stock_pricing():
    if os.system('py F:/code/vdl/jqdata/jq_stock_pricing.py') != 0:
        raise RuntimeError


@task
def jq_macro():
    if os.system('py F:/code/vdl/jqdata/jq_macro.py') != 0:
        raise RuntimeError


@task
def jq_futures():
    if os.system('py F:/code/vdl/jqdata/jq_futures.py') != 0:
        raise RuntimeError


@task
def jq_index_weight():
    if os.system('py F:/code/vdl/jqdata/jq_index_weight.py') != 0:
        raise RuntimeError


@task
def revere_rbics():
    if os.system('py F:/code/vdl/revere/rbics.py') != 0:
        raise RuntimeError


@task
def revere_relationship():
    if os.system('py F:/code/vdl/revere/relationship.py') != 0:
        raise RuntimeError


@task
def product_segment():
    if os.system('py F:/code/vdl/factset/product_segment.py') != 0:
        raise RuntimeError


with Flow("vdl") as flow:
    flow.add_task(insider_trading)
    flow.add_task(csi)
    flow.add_task(executive_change)
    flow.add_task(jq_stock_pricing)
    flow.add_task(jq_macro)
    flow.add_task(jq_futures)
    flow.add_task(jq_index_weight)
    flow.add_task(revere_rbics)
    flow.add_task(revere_relationship)
    flow.add_task(product_segment)

    # set up dependencies
    csi.set_upstream(insider_trading)
    executive_change.set_upstream(csi)
    jq_stock_pricing.set_upstream(executive_change)
    jq_macro.set_upstream(jq_stock_pricing)
    jq_futures.set_upstream(jq_macro)
    jq_index_weight.set_upstream(jq_futures)
    revere_rbics.set_upstream(jq_index_weight)
    revere_relationship.set_upstream(revere_rbics)
    product_segment.set_upstream(revere_relationship)

    # schedule the flow
    schedule = Schedule(clocks=[CronClock("0 7 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
