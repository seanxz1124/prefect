import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def smoothed_forecast():
    if os.system('py F:/code/mdl/diag/smoothed_forecast.py') != 0:
        raise RuntimeError


@task
def forecast_dispersion():
    if os.system('py F:/code/mdl/diag/forecast_dispersion.py') != 0:
        raise RuntimeError


@task
def forecast_autocorrelation():
    if os.system('py F:/code/mdl/diag/forecast_autocorrelation.py') != 0:
        raise RuntimeError


@task
def smoothed_correlation():
    if os.system('py F:/code/mdl/diag/smoothed_correlation.py') != 0:
        raise RuntimeError


@task
def change_dispersion():
    if os.system('py F:/code/mdl/diag/change_dispersion.py') != 0:
        raise RuntimeError


with Flow("forecast_diagnostics") as flow:
    flow.add_task(smoothed_forecast)
    flow.add_task(forecast_dispersion)
    flow.add_task(forecast_autocorrelation)
    flow.add_task(smoothed_correlation)
    flow.add_task(change_dispersion)

    # set up dependencies
    forecast_dispersion.set_upstream(smoothed_forecast)
    forecast_autocorrelation.set_upstream(forecast_dispersion)
    smoothed_correlation.set_upstream(forecast_autocorrelation)
    change_dispersion.set_upstream(smoothed_correlation)

    # schedule the flow
    schedule = Schedule(clocks=[CronClock("50 9 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
