import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def download():
    if os.system('py F:/code/vdl/htsc/com/minute_bar.py') != 0:
        raise RuntimeError


@task
def twap():
    if os.system('py F:/code/vdl/htsc/twap.py') != 0:
        raise RuntimeError


with Flow("htsc_minute_bar") as flow:
    flow.add_task(download)
    flow.add_task(twap)

    # set up dependencies
    twap.set_upstream(download)

    # schedule the flow
    schedule = Schedule(clocks=[CronClock("15 22 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
