import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def wind_universe():
    if os.system('py F:/code/vdl/wind/universe.py') != 0:
        raise RuntimeError


@task
def wind_index():
    if os.system('py F:/code/vdl/wind/index.py') != 0:
        raise RuntimeError


@task
def wind_option():
    if os.system('py F:/code/vdl/wind/option.py') != 0:
        raise RuntimeError


@task
def wind_short_selling():
    if os.system('py F:/code/vdl/wind/short_selling.py') != 0:
        raise RuntimeError


@task
def wind_insider_trading():
    if os.system('py F:/code/vdl/wind/insider_trading.py') != 0:
        raise RuntimeError


@task
def wind_abnormal_trade():
    if os.system('py F:/code/vdl/wind/abnormal_trade.py') != 0:
        raise RuntimeError


@task
def wind_regulation():
    if os.system('py F:/code/vdl/wind/regulation.py') != 0:
        raise RuntimeError


@task
def wind_etf():
    if os.system('py F:/code/vdl/wind/etf.py') != 0:
        raise RuntimeError


@task
def wind_index_weight():
    if os.system('py F:/code/vdl/wind/index_weight.py') != 0:
        raise RuntimeError


@task
def wind_bond():
    if os.system('py F:/code/vdl/wind/bond.py') != 0:
        raise RuntimeError


@task
def wind_promote():
    if os.system('py F:/code/vdl/wind/promote_subitems.py') != 0:
        raise RuntimeError


@task
def cvix():
    if os.system('py F:/code/vdl/wind/cvix.py') != 0:
        raise RuntimeError


with Flow("vdl_wind") as flow:
    flow.add_task(wind_universe)
    flow.add_task(wind_index)
    flow.add_task(wind_option)
    flow.add_task(wind_short_selling)
    flow.add_task(wind_insider_trading)
    flow.add_task(wind_abnormal_trade)
    flow.add_task(wind_regulation)
    flow.add_task(wind_etf)
    flow.add_task(wind_index_weight)
    flow.add_task(wind_bond)
    flow.add_task(wind_promote)
    flow.add_task(cvix)

    # set up dependencies
    wind_index.set_upstream(wind_universe)
    wind_option.set_upstream(wind_index)
    wind_short_selling.set_upstream(wind_option)
    wind_insider_trading.set_upstream(wind_short_selling)
    wind_abnormal_trade.set_upstream(wind_insider_trading)
    wind_regulation.set_upstream(wind_abnormal_trade)
    wind_etf.set_upstream(wind_regulation)
    wind_index_weight.set_upstream(wind_etf)
    wind_bond.set_upstream(wind_index_weight)
    wind_promote.set_upstream(wind_bond)
    cvix.set_upstream(wind_promote)

    # schedule the flow
    schedule = Schedule(clocks=[CronClock("0 7 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
