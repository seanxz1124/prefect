import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def return_st_stock():
    if os.system('py F:/code/mdl/attribution/return_st_stock.py') != 0:
        raise RuntimeError


@task
def return_st_basket():
    if os.system('py F:/code/mdl/attribution/return_st_basket.py') != 0:
        raise RuntimeError


@task
def vm_return_st_bs():
    if os.system('py F:/code/mdl/attribution/vm_return_st_bs.py') != 0:
        raise RuntimeError


@task
def return_f1d_stock():
    if os.system('py F:/code/mdl/attribution/return_f1d_stock.py') != 0:
        raise RuntimeError


with Flow("attribution") as flow:
    flow.add_task(return_st_stock)
    flow.add_task(return_st_basket)
    flow.add_task(vm_return_st_bs)
    flow.add_task(return_f1d_stock)

    # set up dependencies
    return_st_basket.set_upstream(return_st_stock)
    vm_return_st_bs.set_upstream(return_st_basket)
    return_f1d_stock.set_upstream(vm_return_st_bs)

    # schedule the flow
    schedule = Schedule(clocks=[CronClock("55 9 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
