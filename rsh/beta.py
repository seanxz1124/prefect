import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def stack_data():
    if os.system('py F:/code/mdl/beta/stack_historical_data_beta.py') != 0:
        raise RuntimeError


@task
def factor_returns():
    if os.system('py F:/code/mdl/beta/stack_factor_return.py') != 0:
        raise RuntimeError


@task
def beta_signal():
    if os.system('py F:/code/mdl/beta/construct_beta_signal.py') != 0:
        raise RuntimeError


@task
def estimate_simple():
    if os.system('py F:/code/mdl/beta/estimate_beta_simple.py') != 0:
        raise RuntimeError


@task
def estimate_multiple():
    if os.system('py F:/code/mdl/beta/estimate_beta_multiple.py') != 0:
        raise RuntimeError


@task
def update_weight_simple():
    if os.system('py F:/code/mdl/beta/update_beta_weight.py --simple') != 0:
        raise RuntimeError


@task
def update_weight_multiple():
    if os.system('py F:/code/mdl/beta/update_beta_weight.py') != 0:
        raise RuntimeError


with Flow("beta") as flow:
    flow.add_task(stack_data)
    flow.add_task(factor_returns)
    flow.add_task(beta_signal)
    flow.add_task(estimate_simple)
    flow.add_task(estimate_multiple)
    flow.add_task(update_weight_simple)
    flow.add_task(update_weight_multiple)

    # set up dependencies
    factor_returns.set_upstream(stack_data)
    beta_signal.set_upstream(factor_returns)
    estimate_simple.set_upstream(beta_signal)
    estimate_multiple.set_upstream(estimate_simple)
    update_weight_simple.set_upstream(estimate_simple)
    update_weight_multiple.set_upstream(estimate_multiple)

    # schedule the flow
    # in production, we innovate udl beta weights every week
    schedule = Schedule(clocks=[CronClock("30 12 * * FRI", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
