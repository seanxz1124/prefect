import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def download():
    if os.system('py F:/code/vdl/tickdata/download_tickdata.py') != 0:
        raise RuntimeError


with Flow("tickdata") as flow:
    flow.add_task(download)

    # set up dependencies
    # schedule the flow
    schedule = Schedule(clocks=[CronClock("30 22 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
