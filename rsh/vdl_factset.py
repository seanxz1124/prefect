import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def download_stock_data():
    if os.system('py F:/code/vdl/factset/download_stock_data.py') != 0:
        raise RuntimeError


@task
def promote_stock_data_fundamental():
    if os.system('py F:/code/vdl/factset/promote_stock_data_fundamental.py') != 0:
        raise RuntimeError


@task
def promote_stock_data_split():
    if os.system('py F:/code/vdl/factset/promote_stock_data_split.py') != 0:
        raise RuntimeError


@task
def promote_stock_data_nonsplit():
    if os.system('py F:/code/vdl/factset/promote_stock_data_nonsplit.py') != 0:
        raise RuntimeError


@task
def download_macro_data():
    if os.system('py F:/code/vdl/factset/download_macro_data.py') != 0:
        raise RuntimeError


@task
def promote_macro_data():
    if os.system('py F:/code/vdl/factset/promote_macro_data.py') != 0:
        raise RuntimeError


@task
def download_global_data():
    if os.system('py F:/code/vdl/factset/download_global_data.py') != 0:
        raise RuntimeError


@task
def promote_global_data():
    if os.system('py F:/code/vdl/factset/promote_global_data.py') != 0:
        raise RuntimeError


@task
def download_summary_data():
    if os.system('py F:/code/vdl/ibes/download_summary_data.py') != 0:
        raise RuntimeError


@task
def promote_summary_data():
    if os.system('py F:/code/vdl/ibes/promote_summary_data.py') != 0:
        raise RuntimeError


with Flow("vdl_factset") as flow:
    flow.add_task(download_macro_data)
    flow.add_task(promote_macro_data)
    flow.add_task(download_global_data)
    flow.add_task(promote_global_data)
    flow.add_task(download_stock_data)
    flow.add_task(promote_stock_data_fundamental)
    flow.add_task(promote_stock_data_split)
    flow.add_task(promote_stock_data_nonsplit)
    flow.add_task(download_summary_data)
    flow.add_task(promote_summary_data)

    # set up dependencies
    promote_macro_data.set_upstream(download_macro_data)
    download_global_data.set_upstream(promote_macro_data)
    promote_global_data.set_upstream(download_global_data)
    download_stock_data.set_upstream(promote_global_data)
    promote_stock_data_fundamental.set_upstream(download_stock_data)
    promote_stock_data_split.set_upstream(download_stock_data)
    promote_stock_data_nonsplit.set_upstream(download_stock_data)
    download_summary_data.set_upstream(promote_stock_data_split)
    promote_summary_data.set_upstream(download_summary_data)

    # schedule the flow
    schedule = Schedule(clocks=[CronClock("25 7 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
