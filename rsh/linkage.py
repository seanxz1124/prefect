import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def linkage():
    # todo: split this gigantic script
    if os.system('py F:/code/mdl/linkage/construct_linkage_list.py') != 0:
        raise RuntimeError


with Flow("linkage") as flow:
    flow.add_task(linkage)

    # set up dependencies
    # schedule the flow
    schedule = Schedule(clocks=[CronClock("10 10 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
