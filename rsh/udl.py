import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def build_futures_items():
    if os.system('py F:/code/udl/futures/build_futures_items.py') != 0:
        raise RuntimeError


@task
def build_index_items():
    if os.system('py F:/code/udl/index/build_index_items.py') != 0:
        raise RuntimeError


@task
def build_etf_items():
    if os.system('py F:/code/udl/etf/build_etf_items.py') != 0:
        raise RuntimeError


@task
def build_macro_items():
    if os.system('py F:/code/udl/macro/build_macro_items.py') != 0:
        raise RuntimeError


@task
def build_stock_items():
    if os.system('py F:/code/udl/stock/build_stock_items.py') != 0:
        raise RuntimeError


@task
def build_stock_pair_items():
    if os.system('py F:/code/udl/stock_pair/build_stock_pair_items.py') != 0:
        raise RuntimeError


@task
def stock_preprocessor():
    if os.system('py F:/code/udl/stock/stock_preprocessor.py') != 0:
        raise RuntimeError


@task
def stock_to_macro_aggregator():
    if os.system('py F:/code/udl/macro/stock_to_macro_aggregator.py') != 0:
        raise RuntimeError


@task
def macro_preprocessor():
    if os.system('py F:/code/udl/macro/macro_preprocessor.py') != 0:
        raise RuntimeError


@task
def futures_preprocessor():
    if os.system('py F:/code/udl/futures/futures_preprocessor.py') != 0:
        raise RuntimeError


@task
def index_preprocessor():
    if os.system('py F:/code/udl/index/index_preprocessor.py') != 0:
        raise RuntimeError


@task
def stock_to_basket_aggregator():
    if os.system('py F:/code/udl/basket/stock_to_basket_aggregator.py') != 0:
        raise RuntimeError


@task
def build_basket_items():
    if os.system('py F:/code/udl/basket/build_basket_items.py') != 0:
        raise RuntimeError


@task
def stock_fit():
    if os.system('py F:/code/mdl/stock/construct_fit.py') != 0:
        raise RuntimeError


@task
def bill_fit():
    if os.system('py F:/code/mdl/bill/construct_bill_fit.py') != 0:
        raise RuntimeError


@task
def stock_ert():
    if os.system('py F:/code/mdl/stock/construct_ert.py') != 0:
        raise RuntimeError


@task
def otc_ert():
    if os.system('py F:/code/mdl/otc/construct_otc_ert.py') != 0:
        raise RuntimeError


@task
def stock_depvar():
    if os.system('py F:/code/mdl/stock/construct_stock_depvar.py') != 0:
        raise RuntimeError


@task
def basket_depvar():
    if os.system('py F:/code/mdl/basket/construct_basket_depvar.py') != 0:
        raise RuntimeError


@task
def ep_depvar():
    if os.system('py F:/code/mdl/ep/construct_ep_depvar.py') != 0:
        raise RuntimeError


@task
def otc_depvar():
    if os.system('py F:/code/mdl/otc/construct_otc_depvar.py') != 0:
        raise RuntimeError


@task
def trend_depvar():
    if os.system('py F:/code/mdl/trend/construct_trend_depvar.py') != 0:
        raise RuntimeError


@task
def vm_depvar():
    if os.system('py F:/code/mdl/vm/construct_vm_depvar.py') != 0:
        raise RuntimeError


@task
def stock():
    if os.system('py F:/code/mdl/stock/construct_stock_signal.py') != 0:
        raise RuntimeError


@task
def stock_daily():
    if os.system('py F:/code/mdl/daily/construct_daily_signal.py') != 0:
        raise RuntimeError


@task
def stock_tcost():
    if os.system('py F:/code/mdl/tcost/construct_stock_tcost_signal.py') != 0:
        raise RuntimeError


@task
def basket():
    if os.system('py F:/code/mdl/basket/construct_basket_signal.py') != 0:
        raise RuntimeError


@task
def futures():
    if os.system('py F:/code/mdl/futures/construct_futures_signal.py') != 0:
        raise RuntimeError


@task
def etf():
    if os.system('py F:/code/mdl/etf/construct_etf_signal.py') != 0:
        raise RuntimeError


@task
def ep():
    if os.system('py F:/code/mdl/ep/construct_ep_signal.py') != 0:
        raise RuntimeError


@task
def cash():
    if os.system('py F:/code/mdl/cash/construct_cash_signal.py') != 0:
        raise RuntimeError


@task
def otc():
    if os.system('py F:/code/mdl/otc/construct_otc_signal.py') != 0:
        raise RuntimeError


@task
def trend():
    if os.system('py F:/code/mdl/trend/construct_trend_signal.py') != 0:
        raise RuntimeError


@task
def factor_risk():
    if os.system('py F:/code/mdl/risk/construct_factor_risk.py') != 0:
        raise RuntimeError


@task
def residual_risk():
    if os.system('py F:/code/mdl/risk/construct_residual_signal.py') != 0:
        raise RuntimeError


@task
def external():
    if os.system('py F:/code/mdl/external/masterdb_forecast.py') != 0:
        raise RuntimeError


with Flow("udl") as flow:
    flow.add_task(build_futures_items)
    flow.add_task(build_index_items)
    flow.add_task(build_etf_items)
    flow.add_task(build_macro_items)
    flow.add_task(build_stock_items)
    flow.add_task(build_stock_pair_items)
    flow.add_task(stock_preprocessor)
    flow.add_task(stock_to_macro_aggregator)
    flow.add_task(macro_preprocessor)
    flow.add_task(futures_preprocessor)
    flow.add_task(index_preprocessor)
    flow.add_task(stock_to_basket_aggregator)
    flow.add_task(build_basket_items)
    flow.add_task(stock_fit)
    flow.add_task(bill_fit)
    flow.add_task(stock_ert)
    flow.add_task(otc_ert)
    flow.add_task(stock_depvar)
    flow.add_task(basket_depvar)
    flow.add_task(ep_depvar)
    flow.add_task(otc_depvar)
    flow.add_task(vm_depvar)
    flow.add_task(stock)
    flow.add_task(stock_daily)
    flow.add_task(stock_tcost)
    flow.add_task(basket)
    flow.add_task(futures)
    flow.add_task(etf)
    flow.add_task(ep)
    flow.add_task(cash)
    flow.add_task(otc)
    flow.add_task(trend)
    flow.add_task(factor_risk)
    flow.add_task(residual_risk)
    flow.add_task(external)

    # set up dependencies
    stock_preprocessor.set_upstream([build_macro_items, build_stock_items, build_stock_pair_items])
    futures_preprocessor.set_upstream([build_futures_items, build_index_items])
    index_preprocessor.set_upstream([stock_preprocessor, futures_preprocessor])
    stock_to_macro_aggregator.set_upstream(index_preprocessor)
    macro_preprocessor.set_upstream(stock_to_macro_aggregator)
    stock_to_basket_aggregator.set_upstream(macro_preprocessor)
    build_basket_items.set_upstream(stock_to_basket_aggregator)
    stock_fit.set_upstream(build_basket_items)
    bill_fit.set_upstream(build_basket_items)
    stock_ert.set_upstream([stock_fit, bill_fit])
    otc_ert.set_upstream(stock_ert)
    stock_depvar.set_upstream(otc_ert)
    basket_depvar.set_upstream(stock_depvar)
    ep_depvar.set_upstream(basket_depvar)
    otc_depvar.set_upstream(ep_depvar)
    trend_depvar.set_upstream(otc_depvar)
    vm_depvar.set_upstream(trend_depvar)
    stock.set_upstream(vm_depvar)
    stock_daily.set_upstream(stock)
    stock_tcost.set_upstream(stock_daily)
    basket.set_upstream(stock_tcost)
    futures.set_upstream(basket)
    etf.set_upstream(basket)
    ep.set_upstream(basket)
    cash.set_upstream(basket)
    otc.set_upstream(basket)
    trend.set_upstream(otc)
    factor_risk.set_upstream(trend)
    residual_risk.set_upstream(factor_risk)
    external.set_upstream(residual_risk)

    # schedule the flow
    schedule = Schedule(clocks=[CronClock("0 8 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
