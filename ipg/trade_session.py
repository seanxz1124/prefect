import os
import pendulum
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock


@task
def update_holdings_file():
    if os.system('F:/code/pm/update_holdings_file.bat') != 0:
        raise RuntimeError


@task
def daily_performance():
    if os.system('py F:/code/pm/daily_performance.py') != 0:
        raise RuntimeError


@task
def opt_input_group_loading():
    if os.system('py F:/code/pm/input/group_loading.py') != 0:
        raise RuntimeError


@task
def opt_input_holdings():
    if os.system('py F:/code/pm/input/holdings.py') != 0:
        raise RuntimeError


@task
def opt_input_universe():
    if os.system('py F:/code/pm/input/universe.py') != 0:
        raise RuntimeError


@task
def opt_input_short():
    if os.system('py F:/code/pm/input/short.py') != 0:
        raise RuntimeError


@task
def opt_input_instr_restriction():
    if os.system('py F:/code/pm/input/instrument_restriction.py') != 0:
        raise RuntimeError


@task
def rebalance():
    if os.system('py F:/code/pm/rebalance.py') != 0:
        raise RuntimeError


@task
def report():
    if os.system('py F:/code/pm/report.py') != 0:
        raise RuntimeError


@task
def update_optimizer_db():
    if os.system('py F:/code/pm/update_optimizer_db.py') != 0:
        raise RuntimeError


with Flow("trade_session") as flow:
    flow.add_task(update_holdings_file)
    flow.add_task(daily_performance)
    flow.add_task(opt_input_group_loading)
    flow.add_task(opt_input_holdings)
    flow.add_task(opt_input_universe)
    flow.add_task(opt_input_short)
    flow.add_task(opt_input_instr_restriction)
    flow.add_task(rebalance)
    flow.add_task(report)
    flow.add_task(update_optimizer_db)

    # set up dependencies
    daily_performance.set_upstream(update_holdings_file)
    opt_input_group_loading.set_upstream(daily_performance)
    opt_input_holdings.set_upstream(opt_input_group_loading)
    opt_input_universe.set_upstream(opt_input_holdings)
    opt_input_short.set_upstream(opt_input_universe)
    opt_input_instr_restriction.set_upstream(opt_input_short)
    rebalance.set_upstream(opt_input_instr_restriction)
    report.set_upstream(rebalance)
    update_optimizer_db.set_upstream(report)

    # schedule the flow
    schedule = Schedule(clocks=[CronClock("5 9 * * 1-5", start_date=pendulum.now('Asia/Shanghai'))])
    flow.schedule = schedule
