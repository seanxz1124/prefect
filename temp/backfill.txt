
py F:\code\mdl\diag\smoothed_forecast.py --backfill --model_name return_st_stock --half_life 20 --level model
py F:\code\mdl\diag\smoothed_forecast.py --backfill --model_name return_st_basket --half_life 20 --level model
py F:\code\mdl\diag\smoothed_forecast.py --backfill --model_name vm_return_st_bs --half_life 20 --level model
py F:\code\mdl\diag\smoothed_forecast.py --backfill --model_name vm_return_st_ebs --half_life 20 --level model
py F:\code\mdl\diag\smoothed_forecast.py --backfill --model_name return_st_stock --half_life 20 --level signal_group
py F:\code\mdl\diag\smoothed_forecast.py --backfill --model_name return_st_basket --half_life 20 --level signal_group


py F:\code\mdl\diag\smoothed_correlation.py --backfill --model_name return_st_stock --half_life 20 --level model
py F:\code\mdl\diag\smoothed_correlation.py --backfill --model_name return_st_basket --half_life 20 --level model
py F:\code\mdl\diag\smoothed_correlation.py --backfill --model_name vm_return_st_bs --half_life 20 --level model
py F:\code\mdl\diag\smoothed_correlation.py --backfill --model_name return_st_stock --half_life 20 --level signal_group
py F:\code\mdl\diag\smoothed_correlation.py --backfill --model_name return_st_basket --half_life 20 --level signal_group


py F:\code\mdl\diag\forecast_dispersion.py --backfill --model_name return_st_basket --level model
py F:\code\mdl\diag\forecast_dispersion.py --backfill --model_name return_st_basket --level signal_group
py F:\code\mdl\diag\forecast_dispersion.py --backfill --model_name return_st_stock --level model
py F:\code\mdl\diag\forecast_dispersion.py --backfill --model_name return_st_stock --level signal_group
py F:\code\mdl\diag\forecast_dispersion.py --backfill --model_name vm_return_st_bs --level model


py F:\code\mdl\diag\forecast_autocorrelation.py --backfill --model_name return_st_basket --horizon 1 --level model
py F:\code\mdl\diag\forecast_autocorrelation.py --backfill --model_name return_st_basket --horizon 1 --level signal_group
py F:\code\mdl\diag\forecast_autocorrelation.py --backfill --model_name return_st_stock --horizon 1 --level model
py F:\code\mdl\diag\forecast_autocorrelation.py --backfill --model_name return_st_stock --horizon 1 --level signal_group
py F:\code\mdl\diag\forecast_autocorrelation.py --backfill --model_name vm_return_st_bs --horizon 1 --level model


py F:\code\mdl\attribution\return_st_stock.py --backfill
py F:\code\mdl\attribution\return_st_basket.py --backfill
py F:\code\mdl\attribution\vm_return_st_bs.py --backfill
